﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerRotateComponent : MonoBehaviour
{
    public Vector3 rotationDirectionMin = new Vector3(0f, 0f, 0f);
    public Vector3 rotationDirectionMax = new Vector3(0.5f, 0.5f, 0.5f);
    public float smoothTime;
    private float convertedTime = 200;
    private float smooth;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        smooth = Time.deltaTime * smoothTime * convertedTime;
        Vector3 rotationDirection = new Vector3(Random.Range(rotationDirectionMin.x, rotationDirectionMax.x),
            Random.Range(rotationDirectionMin.y, rotationDirectionMax.y),
            Random.Range(rotationDirectionMin.z, rotationDirectionMax.z));
        transform.Rotate(rotationDirection * smooth);
    }
}
