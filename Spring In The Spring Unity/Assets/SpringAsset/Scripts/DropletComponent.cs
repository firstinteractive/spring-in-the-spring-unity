﻿using UnityEngine;

public class DropletComponent : MonoBehaviour
{
    [HideInInspector]
    public Vector3 movingDirection;
    [HideInInspector]
    public Vector3 movingDirectionAfterNoise;
    [HideInInspector]
    public Vector3 spawnPos;
    [HideInInspector]
    public bool isCallNextSpawn = false;
    public bool isCallNextSmoke = false;

    [HideInInspector]
    public float minBoundary;
    [HideInInspector]
    public float maxBoundary;
    [HideInInspector]
    public Vector3 centerCurlty;
    [HideInInspector]
    public float curltyR;
    [HideInInspector]
    public string lastDirection = "left";
    [HideInInspector]
    public bool onCollision = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.tag == "barrier")
    //    {
    //        onCollision = true;
    //        //Debug.Log("onCollision" + onCollision);
    //    }
    //}

    //void OnCollisionExit(Collision collision)
    //{
    //    if (collision.gameObject.tag == "barrier")
    //    {
    //        //onCollision = false;
    //        //Debug.Log("onCollision" + onCollision);
    //    }
    //}

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "barrier")
    //    {
    //        onCollision = true;
    //        Vector3 currentPos = transform.position;
    //        gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(currentPos.x - 100, currentPos.x + 100), 1000, 0));
    //    }
    //}

}
