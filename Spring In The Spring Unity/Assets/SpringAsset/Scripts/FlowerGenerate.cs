﻿using UnityEngine;
using System.Collections;

public class FlowerGenerate : MonoBehaviour
{
    public GameObject petal;
    public float petalSizeMin = 0.003f;
    public float petalSizeMax = 0.006f;
    public int intensity = 10;
    public Vector3 flowerRotateMin = new Vector3(0f, 0f, 0f);
    public Vector3 flowerRotateMax = new Vector3(180f, 0f, 0f);
    public Vector3 bloomForceMin = new Vector3(-300f, -300f, -300f);
    public Vector3 bloomForceMax = new Vector3(500f, 500f, 500f);
    public float forceFlowerFall = 1f;
    public float downForceMin = 300f;
    public float downForceMax = 500f;
    public float timeToDown = 2f;
    public float timeToBorn = 3f;
    public enum Mode
    {
        DISAPPEAR,
        FOLLOW_WATER
    };
    public Mode modeDropDown;

    private MeshFilter pistilMeshFilter;
    private Mesh pistilMesh;
    private Vector3[] verticesft;
    private Vector3[] nor;
    private Vector3 pistilPoint;
    private int verticeLength;
    private float expTime = 0.0f;
    private ArrayList petals = new ArrayList();
    private Vector3 flowerLayerScale;
    private Vector3 flowerLayerPos;
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
    private float maxZ;
    //private float minZ;
    private bool isCollision = false;

    void Start()
    {
        flowerLayerScale = GameObject.Find("FlowerLayer").transform.localScale;
        flowerLayerPos = GameObject.Find("FlowerLayer").transform.position;
        minX = flowerLayerPos.x - flowerLayerScale.x / 2 - 20;
        minY = flowerLayerPos.y - flowerLayerScale.y / 2 - 20;
        maxX = flowerLayerPos.x + flowerLayerScale.x / 2 + 20;
        maxY = flowerLayerPos.y + flowerLayerScale.y / 2 + 20;
        maxZ = 500f;

        pistilMeshFilter = gameObject.GetComponent<MeshFilter>();
        pistilMesh = pistilMeshFilter.mesh;
        verticesft = pistilMesh.vertices;
        verticeLength = verticesft.Length;
        pistilPoint = Vector3.zero;
        nor = pistilMesh.normals;
        for (int i = 0; i < verticeLength; i += intensity)
        {
            pistilPoint = this.transform.TransformPoint(verticesft[i]);
            GameObject currentPetal = Instantiate(petal, pistilPoint, Quaternion.LookRotation(nor[i]), this.transform);
            currentPetal.transform.localScale = new Vector3(Random.Range(petalSizeMin, petalSizeMax), Random.Range(petalSizeMin, petalSizeMax), Random.Range(petalSizeMin, petalSizeMax));
            petals.Add(currentPetal);
        }
        this.transform.eulerAngles = new Vector3(Random.Range(flowerRotateMin.x, flowerRotateMax.x),
            Random.Range(flowerRotateMin.y, flowerRotateMax.y),
            Random.Range(flowerRotateMin.z, flowerRotateMax.z));
    }

    void Update()
    {
        if (petals.Count <= 0)
        {
            //remove pitil when all petals has been destroyed
            Destroy(gameObject);
        }
        for (int i = 0; i < petals.Count; i++)
        {
            //remove petal when out of range
            GameObject valGO = (GameObject)petals[i];
            if (OutOfRange(valGO.transform.position))
            {
                petals.Remove(valGO);
                Destroy(valGO);
            }
        }
    }





    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "barrier" && !isCollision)
        {
            Destroy(gameObject.GetComponent<FlowerRotateComponent>());

            for (int i = 0; i < petals.Count; i++)
            {
                GameObject valGO = (GameObject)petals[i];
                if (valGO != null && valGO.activeInHierarchy)
                {
                    Rigidbody val = valGO.GetComponent<Rigidbody>();
                    if (val != null)
                    {
                        val.AddForce(Random.Range(bloomForceMin.x, bloomForceMax.x) * forceFlowerFall,
                        Random.Range(bloomForceMin.y, bloomForceMax.y) * forceFlowerFall, Random.Range(bloomForceMin.z, bloomForceMax.z) * forceFlowerFall);
                        StartCoroutine(PetalAddForce(i, timeToDown));
                        if(modeDropDown == Mode.DISAPPEAR) StartCoroutine(ResizePetalAfterBloom(valGO, 3f));
                    }
                }
            }
            StartCoroutine(FlowerCountUpdate(timeToBorn));
            isCollision = true;
        }
    }


    private IEnumerator PetalAddForce(int i, float seconds)
    {
        
        //down after second
        GameObject valGO = (GameObject)petals[i];
        if (valGO != null && valGO.activeInHierarchy)
        {
            yield return new WaitForSeconds(seconds);
            Rigidbody val = valGO.GetComponent<Rigidbody>();
            if(val != null)
            {
                val.AddForce(Vector3.down * Random.Range(downForceMin, downForceMax));
            }
        }
    }

    bool OutOfRange (Vector3 val)
    {
        if(maxX < val.x || val.x < minX || maxY < val.y || val.y < minY || maxZ < val.z)
            return true;
        return false;
    }

    private IEnumerator FlowerCountUpdate(float seconds)
    {
        
        yield return new WaitForSeconds(seconds);
        int flowerCount = GameObject.FindGameObjectWithTag("flower").GetComponent<ExtractMap>().flowerCount;
        flowerCount -= 1;
        Debug.Log("Fer" + flowerCount);
        GameObject.FindGameObjectWithTag("flower").GetComponent<ExtractMap>().flowerCount = flowerCount;
    }

    private IEnumerator ResizePetalAfterBloom(GameObject valGO, float seconds)
    {
        while(true)
        {
            
            Vector3 scale = valGO.transform.localScale;
            scale *= 0.9f;
            valGO.transform.localScale = scale;
            Debug.Log("scale" + scale);
            yield return new WaitForSeconds(seconds);
        }
    }
}
