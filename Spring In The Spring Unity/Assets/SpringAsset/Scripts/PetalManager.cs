﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetalManager : MonoBehaviour
{
    public GameObject petalPrefab;
    public int amount = 100;
    public Vector3 movingDirection = new Vector3(0, -20, 0);
    public Vector3 movingNoiseByRatio = new Vector3(0, 0, 0);

    private ArrayList petalGameObjects = new ArrayList();
    private float minX = 0f;
    private float maxX = 0f;
    private float minY = 0f;
    private float maxY = 0f;
    private float minZ = 0f;
    private float maxZ = 0f;

    void Start()
    {
        minX = this.transform.position.x - this.transform.localScale.x / 2;
        minY = this.transform.position.y - this.transform.localScale.y / 2;
        minZ = this.transform.position.z - this.transform.localScale.z / 2;
        maxX = this.transform.position.x + this.transform.localScale.x / 2;
        maxY = this.transform.position.y + this.transform.localScale.y / 2;
        maxZ = this.transform.position.z + this.transform.localScale.z / 2;

        for (int i = 0; i < amount; i++)
        {
            CreateOne();
        }

    }

    void CreateOne()
    {
        CreateOneStraightMode();
    }

    void CreateOneStraightMode()
    {
        Vector3 directionAfterNoise = new Vector3(Random.Range(movingDirection.x * (1 - movingNoiseByRatio.x / 100f), movingDirection.x * (1 + movingNoiseByRatio.x / 100f)),
                Random.Range(movingDirection.y * (1 - movingNoiseByRatio.y / 100f), movingDirection.y * (1 + movingNoiseByRatio.y / 100f)),
                Random.Range(movingDirection.z * (1 - movingNoiseByRatio.z / 100f), movingDirection.z * (1 + movingNoiseByRatio.z / 100f)));

        Vector3 spawnPos = Utils.GenerateSpawnPostion(directionAfterNoise, minX, maxX, minY, maxY, minZ, petalPrefab.transform.localScale.y);

        GameObject petalGameObject = (GameObject)Instantiate(petalPrefab, spawnPos, Quaternion.identity);
        petalGameObject.GetComponent<Rigidbody>().velocity = directionAfterNoise * Random.Range(0.01f, 1f);
        petalGameObject.GetComponent<DropletComponent>().spawnPos = spawnPos;
        petalGameObject.GetComponent<DropletComponent>().movingDirection = movingDirection;
        petalGameObject.GetComponent<DropletComponent>().movingDirectionAfterNoise = directionAfterNoise;
        petalGameObjects.Add(petalGameObject);
    }

    void Update()
    {
        UpdateInStraightMode();
    }

    void UpdateInStraightMode()
    {
        if (petalGameObjects.Count > 0)
        {
            for (int i = 0; i < petalGameObjects.Count; i++)
            {
                GameObject val = (GameObject)petalGameObjects[i];
                if (val != null && val.activeInHierarchy)
                {
                    if (movingDirection.x < 0f)
                    {
                        if (
                        (
                            val.transform.position.y < val.GetComponent<DropletComponent>().spawnPos.y - (maxY - minY) / 4) &&
                            val.GetComponent<DropletComponent>().isCallNextSpawn == false)
                        {
                            CreateOne();
                            val.GetComponent<DropletComponent>().isCallNextSpawn = true;
                        }

                        // Delete if droplet reach the bottom limit
                        if (val.transform.position.x < minX ||
                            val.transform.position.y < minY)
                        {
                            petalGameObjects.Remove(val);
                            Destroy(val);
                        }
                    }
                    else
                    {
                        if (
                        (
                            val.transform.position.y < val.GetComponent<DropletComponent>().spawnPos.y - (maxY - minY) / 4) &&
                            val.GetComponent<DropletComponent>().isCallNextSpawn == false)
                        {
                            CreateOne();
                            val.GetComponent<DropletComponent>().isCallNextSpawn = true;
                        }
                        // Delete if droplet reach the bottom limit
                        if (val.transform.position.x > maxX ||
                            val.transform.position.y < minY)
                        {
                            petalGameObjects.Remove(val);
                            Destroy(val);
                        }
                    }

                }
            }
        }
    }
}
