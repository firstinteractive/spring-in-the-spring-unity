﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

[RequireComponent(typeof(MeshFilter))]
public class RplidarMap : MonoBehaviour
{
   
    public int lidarIndex;
    public bool m_onscan = false;
    public string ip;
    //public string port2 = "COM5";
    public int port = 20108;
    private LidarData[] m_data;
    public Mesh m_mesh;
    public float translateY = 0.01f;
    public float translateX = 0.01f;
    public Vector3 translatePosition = Vector3.zero;
    public float angle;
    public Vector2Int gridSize;
    public GameObject barrier;

    private List<int> m_ind;

    private MeshFilter m_meshfilter;
    private MeshRenderer meshRenderer;

    private Thread m_thread;
    private bool m_datachanged = false;
    public List<Vector3> m_vert;

    public List<Vector2> UVList;
    public int startIndex = 0;
    public int endIndex = 720;

    [SerializeField]
    MeshTopology meshTopology;

    public bool drawMesh = false;

    [SerializeField]
    BoxCollider2D boxRange;

    void Start()
    {
        //ratioOffsets = new float[numberOffset];
        m_meshfilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        m_data = new LidarData[720];

        m_ind = new List<int>();
        m_vert = new List<Vector3>();
        for (int i = 0; i < endIndex - startIndex - 1; i++)
        {
            //m_ind.Add(i);
            m_ind.AddRange(new int[] { 0, i + 1, i + 2 });
        }
        m_mesh = new Mesh();
        m_mesh.MarkDynamic();

        if (m_onscan)
        {
            switch (lidarIndex)
            {
                case 1:
                    //RplidarBinding.OnConnect(port2);
                    RplidarBinding.OnConnectTcp(ip, port);
                    RplidarBinding.StartMotor();
                    m_onscan = RplidarBinding.StartScan();
                    break;
                //case 2:
                //    RplidarBinding2.OnConnectTcp(ip, port);
                //    RplidarBinding2.StartMotor();
                //    m_onscan = RplidarBinding2.StartScan();
                //    break;
            }

            m_thread = new Thread(GenMesh);
            m_thread.Start();
        }
        InvokeRepeating("UpdateAfterSecond", 1.0f, 1f);
        //for(int i = 0; i < 720; i++)
        //{
        //    Vector3 point = new Vector3(UnityEngine.Random.Range(-80, 80), UnityEngine.Random.Range(-160, 160), UnityEngine.Random.Range(0, 0));
        //    Instantiate(barrier, point, Quaternion.identity);
        //}
    }

    void OnDestroy()
    {
        m_thread.Abort();
        switch (lidarIndex)
        {
            case 1:
                RplidarBinding.EndScan();
                RplidarBinding.EndMotor();
                RplidarBinding.OnDisconnect();
                RplidarBinding.ReleaseDrive();
                break;
            //case 2:
            //    RplidarBinding2.EndScan();
            //    RplidarBinding2.EndMotor();
            //    RplidarBinding2.OnDisconnect();
            //    RplidarBinding2.ReleaseDrive();
            //    break;
        }

        m_onscan = false;
    }

    void UpdateAfterSecond()
    {
        if (m_datachanged)
        {
            m_vert.Clear();
            UVList.Clear();

            m_vert.Add(translatePosition);
            UVList.Add(Camera.main.WorldToViewportPoint(translatePosition));
            for (int i = startIndex; i < endIndex; i++)
            {
                Vector3 point = Quaternion.Euler(0, 0, m_data[i].theta - angle) * Vector3.right * m_data[i].distant;  //real point

                float x = m_data[i].distant * Mathf.Sin(Mathf.Deg2Rad * (m_data[i].theta - angle)) * translateX;

                float y = m_data[i].distant * Mathf.Cos(Mathf.Deg2Rad * (m_data[i].theta - angle)) * translateY;

                point = new Vector3(x + translatePosition.x, y + translatePosition.y, translatePosition.z);


                m_vert.Add(point);
                UVList.Add(Camera.main.WorldToViewportPoint(point));

                //var screenPos = Camera.main.WorldToScreenPoint(point);

                //Debug.DrawRay(point, touchRay.direction, Color.red);

                //RaycastHit2D[] hitInfos = Physics2D.RaycastAll(point, Vector3.forward);
                float xMin = boxRange.offset.x - boxRange.size.x / 2;
                float xMax = boxRange.offset.x + boxRange.size.x / 2;
                float yMin = boxRange.offset.y - boxRange.size.y / 2;
                float yMax = boxRange.offset.y + boxRange.size.y / 2;


                //these 2 vectors are particularly used only for WaterYarn
                //Vector2 squareScale = Vector2.one;
                //Vector2 gridSize = new Vector2(35, 20);// Amounts of grid by Vector2


                if (point.x > xMin && point.x < xMax && point.y > yMin && point.y < yMax && i % 2 == 0)
                {
                    Instantiate(barrier, point, Quaternion.Euler(90f, 0f, 0f));

                }
            }
            meshRenderer.enabled = drawMesh;
            if (drawMesh)
            {
                m_mesh.SetVertices(m_vert);
                m_mesh.uv = UVList.ToArray();
                m_mesh.SetIndices(m_ind.ToArray(), meshTopology, 0);

                m_mesh.UploadMeshData(false);
                m_meshfilter.mesh = m_mesh;
            }

            m_datachanged = false;


        }
    }

    void GenMesh()
    {
        while (true)
        {
            int datacount = 0;

            switch (lidarIndex)
            {
                case 1:
                    datacount = RplidarBinding.GetData(ref m_data);
                    break;
                //case 2:
                //    datacount = RplidarBinding2.GetData(ref m_data);
                //    break;
            }


            if (datacount == 0)
            {
                Thread.Sleep(20);
            }
            else
            {
                m_datachanged = true;
            }
        }
    }
  
}
