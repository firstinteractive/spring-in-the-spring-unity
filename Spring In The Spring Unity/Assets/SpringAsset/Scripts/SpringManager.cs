﻿using System.Collections;
using UnityEngine;
using PathCreation;
   [RequireComponent(typeof(PathCreator))]
public class SpringManager : MonoBehaviour
{

    public enum Mode
    {
        STRAIGHT_MODE,
        CURVE_MODE,
       // RIPPLE_MODE,
        BEZIER_MODE
    };
    //public variables
    public Mode modeDropDown;
    public GameObject dropletPrefab;
    public int amount = 100;
    public Vector3 movingDirection = new Vector3(0, -20, 0);
    public Vector3 movingNoiseByRatio = new Vector3(0, 0, 0);
    public float minCurvatureForce = -10f;
    public float maxCurvatureForce = 10f;
    public float width = 1f;
    public float timeLife = 3;
    public float minCurveBound = 5;
    public float maxCurveBound = 10;
    public float curltyInterval = 0.04f;
    public int minAnchorBezier = 10;
    public int maxAnchorBezier = 20;

    // private variables
    private ArrayList dropletGameObjects = new ArrayList();
    private ArrayList dropletPaths = new ArrayList();
    private ArrayList distanceTravelleds = new ArrayList();
    private float minX = 0f;
    private float maxX = 0f;
    private float minY = 0f;
    private float maxY = 0f;
    private float minZ = 0f;
    private float maxZ = 0f;
    private float velocity = -20f;

    // Start is called before the first frame update
    void Start()
    {
        minX = this.transform.position.x - this.transform.localScale.x / 2;
        minY = this.transform.position.y - this.transform.localScale.y / 2;
        minZ = this.transform.position.z - this.transform.localScale.z / 2;
        maxX = this.transform.position.x + this.transform.localScale.x / 2;
        maxY = this.transform.position.y + this.transform.localScale.y / 2;
        maxZ = this.transform.position.z + this.transform.localScale.z / 2;

        velocity = movingDirection.y;
        // Generic
        for (int i = 0; i < amount; i++)
        {
            CreateOne();
        }

        // Mode = STRAIGHT_MODE
        // Mode = CURVE_MODE
        // Mode = RIPPLE_MODE
        //if (modeDropDown == Mode.RIPPLE_MODE)
        //{
        //    InvokeRepeating("Timer", 0.0f, curltyInterval);
        //}
    }

    void CreateOne()
    {
        if (modeDropDown == Mode.STRAIGHT_MODE)
        {
            CreateOneStraightMode();
        }
        //else if (modeDropDown == Mode.CURVE_MODE)
        //{
        //    CreateOneCurveMode();
        //}
        //else if (modeDropDown == Mode.RIPPLE_MODE)
        //{
        //    CreateOneRippleMode();
        //}
        else if (modeDropDown == Mode.BEZIER_MODE)
        {
            CreateOneBezierMode();
        }
    }

    void CreateOneStraightMode()
    {
        Vector3 directionAfterNoise = new Vector3(Random.Range(movingDirection.x * (1 - movingNoiseByRatio.x / 100f), movingDirection.x * (1 + movingNoiseByRatio.x / 100f)),
                Random.Range(movingDirection.y * (1 - movingNoiseByRatio.y / 100f), movingDirection.y * (1 + movingNoiseByRatio.y / 100f)),
                Random.Range(movingDirection.z * (1 - movingNoiseByRatio.z / 100f), movingDirection.z * (1 + movingNoiseByRatio.z / 100f)));
        
        Vector3 spawnPos = Utils.GenerateSpawnPostion(directionAfterNoise, minX, maxX, minY, maxY, minZ, timeLife);

        GameObject dropletGameObject = (GameObject)Instantiate(dropletPrefab, spawnPos, Quaternion.identity);
        //dropletGameObject.GetComponent<Rigidbody>().AddForce(directionAfterNoise);
        dropletGameObject.GetComponentInChildren<TrailRenderer>().time = timeLife;
        dropletGameObject.GetComponentInChildren<TrailRenderer>().widthMultiplier = width;
        dropletGameObject.GetComponent<Rigidbody>().velocity = directionAfterNoise;
        dropletGameObject.GetComponent<DropletComponent>().spawnPos = spawnPos;
        dropletGameObject.GetComponent<DropletComponent>().movingDirection = movingDirection;
        dropletGameObject.GetComponent<DropletComponent>().movingDirectionAfterNoise = directionAfterNoise;
        dropletGameObjects.Add(dropletGameObject);
    }

    private void CreateStraightModeWithBezier()
    {
        Vector3 directionAfterNoise = new Vector3(Random.Range(movingDirection.x * (1 - movingNoiseByRatio.x / 100f), movingDirection.x * (1 + movingNoiseByRatio.x / 100f)),
                Random.Range(movingDirection.y * (1 - movingNoiseByRatio.y / 100f), movingDirection.y * (1 + movingNoiseByRatio.y / 100f)),
                Random.Range(movingDirection.z * (1 - movingNoiseByRatio.z / 100f), movingDirection.z * (1 + movingNoiseByRatio.z / 100f)));

        Vector3[] waypointsForGenerateLine = new Vector3[5];
        waypointsForGenerateLine[0] = new Vector3(-80, 160);
        waypointsForGenerateLine[1] = new Vector3(-50, 180);
        waypointsForGenerateLine[2] = new Vector3(-10, 165);
        waypointsForGenerateLine[3] = new Vector3(30, 200);
        waypointsForGenerateLine[4] = new Vector3(80, 160);
        BezierPath bezierPath = new BezierPath(waypointsForGenerateLine, false, PathSpace.xyz);        GameObject dropletPath = new GameObject("Droplet Path");        dropletPath.AddComponent<PathCreator>();
        dropletPath.GetComponent<PathCreator>().bezierPath = bezierPath;

        float spacing = 1f;
        float dst = 0;
        VertexPath vertexPath = dropletPath.GetComponent<PathCreator>().path;
        while (dst < vertexPath.length)
        {
            Vector3 point = vertexPath.GetPointAtDistance(dst);
            Quaternion rot = vertexPath.GetRotationAtDistance(dst);
            GameObject dropletGameObject = (GameObject)Instantiate(dropletPrefab, point, rot);
            dropletGameObject.GetComponent<Rigidbody>().AddForce(directionAfterNoise);
            dropletGameObject.GetComponentInChildren<TrailRenderer>().time = timeLife;
            dropletGameObject.GetComponentInChildren<TrailRenderer>().widthMultiplier = width;
            dropletGameObject.GetComponent<Rigidbody>().velocity = directionAfterNoise;
            dropletGameObject.GetComponent<DropletComponent>().spawnPos = point;
            dropletGameObject.GetComponent<DropletComponent>().movingDirection = movingDirection;
            dropletGameObject.GetComponent<DropletComponent>().movingDirectionAfterNoise = directionAfterNoise;
            dropletGameObjects.Add(dropletGameObject);
            dst += spacing;
        }
    }

    void CreateOneCurveMode()
    {
        Vector3 directionAfterNoise = new Vector3(Random.Range(movingDirection.x * (1 - movingNoiseByRatio.x / 100f), movingDirection.x * (1 + movingNoiseByRatio.x / 100f)),
                Random.Range(movingDirection.y * (1 - movingNoiseByRatio.y / 100f), movingDirection.y * (1 + movingNoiseByRatio.y / 100f)),
                Random.Range(movingDirection.z * (1 - movingNoiseByRatio.z / 100f), movingDirection.z * (1 + movingNoiseByRatio.z / 100f)));

        Vector3 spawnPos = Utils.GenerateSpawnPostion(directionAfterNoise, minX, maxX, minY, maxY, minZ, timeLife);
        AnimationCurve curve = new AnimationCurve();
        curve.AddKey(0.0f, 1.0f);
        curve.AddKey(1.0f, 0.0f);

        GameObject projectile = (GameObject)Instantiate(dropletPrefab, spawnPos, Quaternion.identity);
        projectile.GetComponent<Rigidbody>().useGravity = true;
        projectile.GetComponentInChildren<TrailRenderer>().time = timeLife;
        projectile.GetComponentInChildren<TrailRenderer>().widthCurve = curve;
        projectile.GetComponentInChildren<TrailRenderer>().widthMultiplier = width;

        float randBound = Random.Range(minCurveBound, maxCurveBound);

        projectile.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(minCurvatureForce, maxCurvatureForce), 0f, 0f));
        projectile.GetComponentInChildren<DropletComponent>().minBoundary = projectile.transform.position.x - randBound;
        projectile.GetComponentInChildren<DropletComponent>().maxBoundary = projectile.transform.position.x + randBound;

        dropletGameObjects.Add(projectile);
    }

    void CreateOneRippleMode()
    {
        Vector3 directionAfterNoise = new Vector3(Random.Range(movingDirection.x * (1 - movingNoiseByRatio.x / 100f), movingDirection.x * (1 + movingNoiseByRatio.x / 100f)),
                Random.Range(movingDirection.y * (1 - movingNoiseByRatio.y / 100f), movingDirection.y * (1 + movingNoiseByRatio.y / 100f)),
                Random.Range(movingDirection.z * (1 - movingNoiseByRatio.z / 100f), movingDirection.z * (1 + movingNoiseByRatio.z / 100f)));

        Vector3 spawnPos = Utils.GenerateSpawnPostion(directionAfterNoise, minX, maxX, minY, maxY, minZ, timeLife);
        AnimationCurve curve = new AnimationCurve();
        curve.AddKey(0.0f, 1.0f);
        curve.AddKey(1.0f, 0.0f);

        GameObject projectile = (GameObject)Instantiate(dropletPrefab, spawnPos, Quaternion.identity);
        projectile.GetComponent<Rigidbody>().useGravity = true;
        projectile.GetComponentInChildren<TrailRenderer>().time = timeLife;
        projectile.GetComponentInChildren<TrailRenderer>().widthCurve = curve;
        projectile.GetComponentInChildren<TrailRenderer>().widthMultiplier = width;

        float randBound = Random.Range(minCurveBound, maxCurveBound);

        projectile.GetComponentInChildren<DropletComponent>().curltyR = randBound;
        projectile.GetComponentInChildren<DropletComponent>().centerCurlty.x = projectile.transform.position.x;
        projectile.GetComponentInChildren<DropletComponent>().centerCurlty.y = projectile.transform.position.y - randBound;

        dropletGameObjects.Add(projectile);
    }

    void CreateOneBezierMode()
    {
        Vector3 directionAfterNoise = new Vector3(Random.Range(movingDirection.x * (1 - movingNoiseByRatio.x / 100f), movingDirection.x * (1 + movingNoiseByRatio.x / 100f)),
                Random.Range(movingDirection.y * (1 - movingNoiseByRatio.y / 100f), movingDirection.y * (1 + movingNoiseByRatio.y / 100f)),
                Random.Range(movingDirection.z * (1 - movingNoiseByRatio.z / 100f), movingDirection.z * (1 + movingNoiseByRatio.z / 100f)));

        Vector3 spawnPos = Utils.GenerateSpawnPostion(directionAfterNoise, minX, maxX, minY, maxY, minZ, timeLife);

        GameObject dropletGameObject = (GameObject)Instantiate(dropletPrefab, spawnPos, Quaternion.identity);
        dropletGameObject.GetComponent<Rigidbody>().AddForce(directionAfterNoise);
        dropletGameObject.GetComponentInChildren<TrailRenderer>().time = timeLife;
        dropletGameObject.GetComponentInChildren<TrailRenderer>().widthMultiplier = width;
        dropletGameObject.GetComponent<Rigidbody>().velocity = directionAfterNoise;
        dropletGameObject.GetComponent<DropletComponent>().spawnPos = spawnPos;
        dropletGameObject.GetComponent<DropletComponent>().movingDirection = movingDirection;
        dropletGameObject.GetComponent<DropletComponent>().movingDirectionAfterNoise = directionAfterNoise;

        //create path
        Vector3[] waypoints = generatePointsBezier(Random.Range(minAnchorBezier, maxAnchorBezier), spawnPos, minCurveBound, maxCurveBound, minY);
        BezierPath bezierPath = new BezierPath(waypoints, false, PathSpace.xyz);        GameObject dropletPath = new GameObject("Droplet Path");        dropletPath.AddComponent<PathCreator>();
        dropletPath.GetComponent<PathCreator>().bezierPath = bezierPath;

        dropletGameObjects.Add(dropletGameObject);
        dropletPaths.Add(dropletPath);
        distanceTravelleds.Add(0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (modeDropDown == Mode.STRAIGHT_MODE)
        {
            UpdateInStraightMode();
        }
        //else if (modeDropDown == Mode.CURVE_MODE)
        //{
        //    UpdateInCurveMode();
        //}
        //else if (modeDropDown == Mode.RIPPLE_MODE)
        //{
        //    UpdateInRippleMode();
        //}
        else if (modeDropDown == Mode.BEZIER_MODE)
        {
            UpdateInBezierMode();
        }
    }

    void UpdateInStraightMode()
    {
        if (dropletGameObjects.Count > 0)
        {
            for (int i = 0; i < dropletGameObjects.Count; i++)
            {
                GameObject val = (GameObject)dropletGameObjects[i];
                movingDirection = val.GetComponent<DropletComponent>().movingDirection;
                if (val != null && val.activeInHierarchy)
                {
                    //if (val.GetComponent<Rigidbody>().velocity.x > val.GetComponent<DropletComponent>().movingDirectionAfterNoise.x)
                    //{
                    //    val.GetComponent<Rigidbody>().velocity = new Vector3(val.GetComponent<DropletComponent>().movingDirectionAfterNoise.x, val.GetComponent<Rigidbody>().velocity.y, 0f);
                    //}

                    if (val.GetComponent<Rigidbody>().velocity.y > val.GetComponent<DropletComponent>().movingDirectionAfterNoise.y)
                    {
                        val.GetComponent<Rigidbody>().velocity = new Vector3(val.GetComponent<Rigidbody>().velocity.x, val.GetComponent<DropletComponent>().movingDirectionAfterNoise.y, 0f);
                    }

                    //if (val.GetComponent<DropletComponent>().onCollision)
                    //{
                    //    val.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    //    val.GetComponent<Rigidbody>().velocity = movingDirection;
                    //}

                    float TrailSizeX = Utils.TrailSizeX(timeLife, movingDirection.x);
                    float TrailSizeY = Utils.TrailSizeY(timeLife, movingDirection.y);
                    
                    if (movingDirection.x < 0f)
                    {
                        if (
                        (val.transform.position.x < val.GetComponent<DropletComponent>().spawnPos.x + TrailSizeX ||
                            val.transform.position.y < val.GetComponent<DropletComponent>().spawnPos.y + TrailSizeY) &&
                            val.GetComponent<DropletComponent>().isCallNextSpawn == false)
                        {
                            CreateOne();
                            val.GetComponent<DropletComponent>().isCallNextSpawn = true;
                        }

                        // Delete if droplet reach the bottom limit
                        if (val.transform.position.x < minX + TrailSizeX ||
                            val.transform.position.y < minY + TrailSizeY)
                        {
                            dropletGameObjects.Remove(val);
                            Destroy(val);
                        }
                    } else
                    {
                        if (
                        (
                        val.transform.position.x > val.GetComponent<DropletComponent>().spawnPos.x + TrailSizeX ||
                            val.transform.position.y < val.GetComponent<DropletComponent>().spawnPos.y + TrailSizeY) &&
                            val.GetComponent<DropletComponent>().isCallNextSpawn == false)
                        {
                            CreateOne();
                            val.GetComponent<DropletComponent>().isCallNextSpawn = true;
                        }
                        // generate smoke
                        int smokeAmount = GameObject.FindGameObjectsWithTag("smoke").Length;
                        if (val.transform.position.y < minY && val.GetComponent<DropletComponent>().isCallNextSmoke == false && smokeAmount < 10)
                        {
                            GameObject.Find("SmokeLayer").GetComponent<SmokeManager>().InstanceSmoke(val.transform.position);
                            val.GetComponent<DropletComponent>().isCallNextSmoke = true;
                        }
                        // Delete if droplet reach the bottom limit
                        if (val.transform.position.x > maxX + TrailSizeX ||
                            val.transform.position.y < minY + TrailSizeY)
                        {
                            dropletGameObjects.Remove(val);
                            Destroy(val);
                        }
                    }
                    
                }
            }
        }
    }

    void UpdateInCurveMode()
    {
        if (dropletGameObjects.Count > 0)
        {
            for (int i = 0; i < dropletGameObjects.Count; i++)
            {
                GameObject val = (GameObject)dropletGameObjects[i];
                if (val != null && val.activeInHierarchy)
                {
                    val.GetComponent<Rigidbody>().velocity = new Vector3(val.GetComponent<Rigidbody>().velocity.x, velocity, 0f);
                    if (val.GetComponent<Rigidbody>().velocity.y > velocity)
                    {
                        val.GetComponent<Rigidbody>().velocity = new Vector3(val.GetComponent<Rigidbody>().velocity.x, velocity, 0f);
                    }
                    //if (val.GetComponentInChildren<DropletComponent>().onCollision)
                    //{
                    //    val.GetComponent<Rigidbody>().velocity = new Vector3(val.GetComponent<Rigidbody>().velocity.x, 1f, 0f);
                    //}
                    if (val.transform.position.y < minY + (Utils.TrailSizeY(timeLife, velocity)))
                    {
                        dropletGameObjects.Remove(val);
                        Destroy(val);
                        CreateOne();
                    }

                    //if (modeDropDown == Mode.CURVE_MODE)
                    //{
                    //    if (val.transform.position.x <= val.GetComponentInChildren<DropletComponent>().minBoundary)
                    //    {
                    //        Vector3 force = new Vector3(Random.Range(0f, maxCurvatureForce), 0f, 0f);
                    //        val.GetComponent<Rigidbody>().AddForce(force);
                    //    }
                    //    else if (val.transform.position.x >= val.GetComponentInChildren<DropletComponent>().maxBoundary)
                    //    {
                    //        Vector3 force = new Vector3(Random.Range(minCurvatureForce, 0f), 0f, 0f);
                    //        val.GetComponent<Rigidbody>().AddForce(force);
                    //    }
                    //}
                }
            }
        }
    }

    void UpdateInRippleMode()
    {
        if (dropletGameObjects.Count > 0)
        {
            //foreach (GameObject val in objs)
            for (int i = 0; i < dropletGameObjects.Count; i++)
            {
                GameObject val = (GameObject)dropletGameObjects[i];
                if (val != null && val.activeInHierarchy)
                {
                    val.GetComponent<Rigidbody>().velocity = new Vector3(val.GetComponent<Rigidbody>().velocity.x, velocity, 0f);
                    if (val.GetComponent<Rigidbody>().velocity.y > velocity)
                    {
                        val.GetComponent<Rigidbody>().velocity = new Vector3(val.GetComponent<Rigidbody>().velocity.x, velocity, 0f);
                    }

                    if (val.transform.position.y < minY + (Utils.TrailSizeY(timeLife, velocity)))
                    {
                        dropletGameObjects.Remove(val);
                        Destroy(val);
                        CreateOne();
                    }
                }
            }
        }
    }

    void UpdateInBezierMode()
    {
        if (dropletGameObjects.Count > 0)
        {
            for (int i = 0; i < dropletGameObjects.Count; i++)
            {
                GameObject val = (GameObject)dropletGameObjects[i];
                GameObject dropletPath = (GameObject)dropletPaths[i];
                float distanceTravelled = (float)distanceTravelleds[i];
                if (val != null && val.activeInHierarchy && dropletPath != null)
                {
                    if(!val.GetComponent<DropletComponent>().onCollision)
                    {
                        distanceTravelled += Mathf.Abs(movingDirection.y) * Time.deltaTime;
                        distanceTravelleds[i] = distanceTravelled;
                        val.transform.position = dropletPath.GetComponent<PathCreator>().path.GetPointAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
                        //val.transform.rotation = dropletPath.GetComponent<PathCreator>().path.GetRotationAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
                    }
                    else
                    {
                        // tim vi tri neo gan truoc no
                        // tao path moi tu vi tri neo do
                        // cap nhat mang path voi path vua tao
                        // cho di chuyen theo path moi
                        val.GetComponent<Rigidbody>().velocity = Vector3.zero;
                        val.GetComponent<Rigidbody>().velocity = movingDirection;

                    }

                    float TrailSizeY = Utils.TrailSizeY(timeLife, movingDirection.y);
                    if (val.transform.position.y < (val.GetComponent<DropletComponent>().spawnPos.y + TrailSizeY) &&
                        val.GetComponent<DropletComponent>().isCallNextSpawn == false)
                    {
                        CreateOne();
                        val.GetComponent<DropletComponent>().isCallNextSpawn = true;
                    }
                    //// generate smoke
                    int smokeAmount = GameObject.FindGameObjectsWithTag("smoke").Length;
                    if (val.transform.position.y < minY && val.GetComponent<DropletComponent>().isCallNextSmoke == false && smokeAmount < 10)
                    {
                        GameObject.Find("SmokeLayer").GetComponent<SmokeManager>().InstanceSmoke(val.transform.position);
                        val.GetComponent<DropletComponent>().isCallNextSmoke = true;
                    }
                    if (val.transform.position.y < minY + (Utils.TrailSizeY(timeLife, movingDirection.y)))
                    {
                       
                        Destroy(val);
                        Destroy(dropletPath);
                        dropletGameObjects.Remove(val);
                        dropletPaths.Remove(dropletPath);
                        distanceTravelleds.Remove(distanceTravelled);
                    }

                }
            }
        }
    }

    private IEnumerator DropletAddForce(GameObject go, float seconds)
    {

        //down after second
        if (go != null && go.activeInHierarchy)
        {
            yield return new WaitForSeconds(seconds);
            Rigidbody val = go.GetComponent<Rigidbody>();
            if (val != null)
            {
                val.AddForce(Vector3.down * Random.Range(100, 100));
            }
        }
    }
    private float NextPos(float pos) {
        float newPos = pos + velocity * curltyInterval;
        return newPos;
    }

    public void Timer()
    {
        if (dropletGameObjects.Count > 0)
        {
            foreach (GameObject val in dropletGameObjects)
            {
                if (val != null && val.activeInHierarchy)
                {
                    float curY = val.transform.position.y;
                    float Y = NextPos(curY);
                    float R = val.GetComponentInChildren<DropletComponent>().curltyR;
                    float xI = val.GetComponentInChildren<DropletComponent>().centerCurlty.x;
                    float yI = val.GetComponentInChildren<DropletComponent>().centerCurlty.y;
                    float X;

                    if (val.GetComponentInChildren<DropletComponent>().lastDirection == "left")
                    {
                        X = xI - Mathf.Sqrt(R * R - ((Y - yI) * (Y - yI)));
                    }
                    else
                    {
                        X = xI + Mathf.Sqrt(R * R - ((Y - yI) * (Y - yI)));
                    }
                    if (!float.IsNaN(X))
                    {
                        val.transform.position = new Vector3(X, Y, 0f);
                    }

                    else
                    {
                        float randBound = Random.Range(minCurveBound, maxCurveBound);
                        val.GetComponentInChildren<DropletComponent>().curltyR = randBound;
                        val.GetComponentInChildren<DropletComponent>().centerCurlty.x = val.transform.position.x;
                        val.GetComponentInChildren<DropletComponent>().centerCurlty.y = val.transform.position.y - randBound;
                        if (val.GetComponentInChildren<DropletComponent>().lastDirection == "left")
                        {
                            val.GetComponentInChildren<DropletComponent>().lastDirection = "right";
                        }
                        else
                        {
                            val.GetComponentInChildren<DropletComponent>().lastDirection = "left";
                        }
                    }
                }
            }
        }
    }

    public Vector3[] generatePointsBezier(int _length, Vector3 spawnPos, float _minX, float _maxX, float _minY)
    {
        Vector3[] points = new Vector3[_length];
        points[0] = spawnPos;
        float steps = (spawnPos.y - _minY + Mathf.Abs(Utils.TrailSizeY(timeLife, movingDirection.y))) / _length;
        float minRangeY = spawnPos.y - steps;
        float maxRangeY;
        for (int i = 1; i < _length - 1; i++)
        {
            maxRangeY = minRangeY;
            minRangeY = minRangeY - steps;
            points[i].x = Random.Range(spawnPos.x + _minX, spawnPos.x + _maxX);
            float currentY = Random.Range(minRangeY, maxRangeY);
            points[i].y = currentY;
            points[i].z = spawnPos.z;
        }
        points[_length - 1] = new Vector3(Random.Range(spawnPos.x + _minX, spawnPos.x + _maxX), -10 + _minY + (Utils.TrailSizeY(timeLife, movingDirection.y)), spawnPos.z);
        return points;
    }
}
