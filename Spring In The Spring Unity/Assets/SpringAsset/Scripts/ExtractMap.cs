﻿using UnityEngine;
using UnityEngine.UI;

public class ExtractMap : MonoBehaviour
{
    public GameObject flower;
    public int row = 10;
    public int col = 10;

    Texture heightMap;
    Texture2D tex2D;
    int mapWidth;
    int mapHeight;
    int flowerTotal = 0;

    [HideInInspector]
    public int flowerCount;

    void Start()
    {
        heightMap = GetComponent<RawImage>().texture;
        tex2D = (Texture2D)heightMap;
        mapWidth = tex2D.width;
        mapHeight = tex2D.height;
        int scaleW = CalculateCellSize(mapWidth, row);
        int scaleH = CalculateCellSize(mapHeight, col);
        for (int i = 0; i < col; i++)
        {
            for(int j = 0; j < row; j++)
            {
                int tempPosY = Random.Range(scaleH * i, scaleH * (i + 1));
                int tempPosX = Random.Range(scaleW * j, scaleW * (j + 1));
                Color pixel_colour = tex2D.GetPixel(tempPosX, tempPosY);
                float rate = (pixel_colour.r + pixel_colour.g + pixel_colour.b) / 3;
                
                if(rate == 1f)
                {
                    //Debug.Log("OK");
                    Vector3 genPos = MapPointToScreenPoint(new Vector2(tempPosX, tempPosY), mapWidth, mapHeight, 160, 320);
                    GameObject newFlower = Instantiate(flower, genPos, Quaternion.Euler(0f, 180f, Random.Range(0f, 180f)));
                    flowerTotal += 1;
                } else if(rate == 0f)
                {
                    //Debug.Log("Failed");
                } else
                {
                    float temp = Random.Range(0, 3);

                    if(0 <= temp && temp <= (rate * 3))
                    {
                       // Debug.Log("OK");
                        Vector3 genPos = MapPointToScreenPoint(new Vector2(tempPosX, tempPosY), mapWidth, mapHeight, 160, 320);
                        GameObject newFlower = Instantiate(flower, genPos, Quaternion.Euler(0f, 180f, Random.Range(0f, 180f)));
                        flowerTotal += 1;
                    } else {
                        //Debug.Log("Failed");
                    }
                }
            }
        }
        flowerCount = flowerTotal;
    }

    Vector3 MapPointToScreenPoint(Vector2 point, float mapW, float mapH, float screenW, float screenH)
    {
        float rateW = screenW / mapW ;
        float rateH = screenH / mapH ;
        float _x = point.x * rateW - screenW / 2;
        float _y = point.y * rateH - screenH / 2;
        return new Vector3(_x, _y, gameObject.transform.parent.parent.position.z);
    }

    int CalculateCellSize(int screenScale, int numCell)
    {
        int size = screenScale / numCell;
        return size;
    }

    private void Update()
    {
        if (flowerCount < flowerTotal)
        {
            int flowers = flowerTotal - flowerCount;
            for(int i = 0; i < flowers; i++)
            {
                int tempPosY = Random.Range(0, mapHeight);
                int tempPosX = Random.Range(0, mapWidth);

                Color pixel_colour = tex2D.GetPixel(tempPosX, tempPosY);
                float rate = (pixel_colour.r + pixel_colour.g + pixel_colour.b) / 3;

                if (rate == 1f)
                {
                    //Debug.Log("OK");
                    Vector3 genPos = MapPointToScreenPoint(new Vector2(tempPosX, tempPosY), mapWidth, mapHeight, 160, 320);
                    GameObject newFlower = Instantiate(flower, genPos, Quaternion.Euler(0f, 180f, Random.Range(0f, 180f)));
                    flowerCount += 1;
                }
                else if (rate == 0f)
                {
                    //Debug.Log("Failed");
                }
                else
                {
                    float temp = Random.Range(0, 3);

                    if (0 <= temp && temp <= (rate * 3))
                    {
                        // Debug.Log("OK");
                        Vector3 genPos = MapPointToScreenPoint(new Vector2(tempPosX, tempPosY), mapWidth, mapHeight, 160, 320);
                        GameObject newFlower = Instantiate(flower, genPos, Quaternion.Euler(0f, 180f, Random.Range(0f, 180f)));
                        flowerCount += 1;
                    }
                    else
                    {
                        //Debug.Log("Failed");
                    }
                }
            }
        }
    }
}
