﻿using UnityEngine;

public class BubbleComponent : MonoBehaviour
{
    [HideInInspector]
    public Vector3 movingDirection;
    [HideInInspector]
    public Vector3 movingDirectionAfterNoise;
    [HideInInspector]
    public Vector3 spawnPos;
    [HideInInspector]
    public bool isCallNextSpawn = false;
    [HideInInspector]
    public bool onCollision = false;
    [HideInInspector]
    public Vector3 collisionPoint;
    [HideInInspector]
    public bool collisionGen = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "barrier")
        {
            onCollision = true;
            Vector3 currentPos = transform.position;
            collisionPoint = currentPos;
            gameObject.GetComponent<Rigidbody>().AddForce(Random.Range(-75f, 75f), 1000, 0);
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "barrier")
        {
            onCollision = false;
        }
    }


}
