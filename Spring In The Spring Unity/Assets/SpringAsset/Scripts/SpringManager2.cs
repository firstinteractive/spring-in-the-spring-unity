﻿using System.Collections;
using UnityEngine;
using PathCreation;
   [RequireComponent(typeof(PathCreator))]
public class SpringManager2 : MonoBehaviour
{

    public GameObject dropletPrefab;
    public float intensityRateMin = 1;
    public float intensityRateMax = 3;
    public int pointGenNumMin = 5;
    public int pointGenNumMax = 10;
    public float oscillateBezierFollowY = 50f;
    public Vector3 movingDirection = new Vector3(0, -20, 0);
    public Vector3 movingNoiseByRatio = new Vector3(0, 0, 0);
    public float width = 1f;
    public float timeLife = 3;

    // private variables
    private ArrayList dropletGameObjects = new ArrayList();
    private float minX = 0f;
    private float maxX = 0f;
    private float minY = 0f;
    private float maxY = 0f;
    private float minZ = 0f;
    private float maxZ = 0f;
    //private float velocity = -20f;

    // Start is called before the first frame update
    void Start()
    {
        minX = this.transform.position.x - this.transform.localScale.x / 2;
        minY = this.transform.position.y - this.transform.localScale.y / 2;
        minZ = this.transform.position.z - this.transform.localScale.z / 2;
        maxX = this.transform.position.x + this.transform.localScale.x / 2;
        maxY = this.transform.position.y + this.transform.localScale.y / 2;
        maxZ = this.transform.position.z + this.transform.localScale.z / 2;

        // velocity = movingDirection.y;

        //CreateStraightModeWithBezier();
        
        InvokeRepeating("CreateStraightModeWithBezier", 0.01f, 3f);
    }

    public Vector3[] generatePointsBezier(int _length, float _minX, float _maxX, float _minY, float _maxY)
    {
        Vector3[] points = new Vector3[_length];
        points[0] = new Vector3(_minX, Random.Range(_minY, _maxY), 0f);
        int lengthHaft = _length / 2;
        float _tempX = _minX;
        for (int i = 1; i < lengthHaft; i++)
        {
            float tempX = Random.Range(_tempX, 0f);
            points[i] = new Vector3(tempX, Random.Range(_minY, _maxY), 0);
            //Debug.Log(points[i]);
            _tempX = tempX;
        }
        for(int i = lengthHaft; i < _length - 1; i++)
        {
            float tempX = Random.Range(_tempX, _maxX);
            points[i] = new Vector3(tempX, Random.Range(_minY, _maxY), 0);
            //Debug.Log(points[i]);
            _tempX = tempX;
        }
        points[_length - 1] = new Vector3(_maxX, Random.Range(_minY, _maxY), 0f);
        
        return points;
    }

    private void CreateStraightModeWithBezier()
    {
        Vector3 directionAfterNoise = new Vector3(Random.Range(movingDirection.x * (1 - movingNoiseByRatio.x / 100f), movingDirection.x * (1 + movingNoiseByRatio.x / 100f)),
                Random.Range(movingDirection.y * (1 - movingNoiseByRatio.y / 100f), movingDirection.y * (1 + movingNoiseByRatio.y / 100f)),
                Random.Range(movingDirection.z * (1 - movingNoiseByRatio.z / 100f), movingDirection.z * (1 + movingNoiseByRatio.z / 100f)));

        Vector3[] waypointsForGenerateLine = generatePointsBezier((Random.Range(pointGenNumMin, pointGenNumMax)), minX, maxX, maxY, maxY + oscillateBezierFollowY);
        BezierPath bezierPath = new BezierPath(waypointsForGenerateLine, false, PathSpace.xyz);        GameObject dropletPath = new GameObject("Droplet Path");        dropletPath.AddComponent<PathCreator>();
        dropletPath.GetComponent<PathCreator>().bezierPath = bezierPath;

        
        float dst = 0;
        VertexPath vertexPath = dropletPath.GetComponent<PathCreator>().path;
        while (dst < vertexPath.length)
        {
            float spacing = Random.Range(intensityRateMin, intensityRateMax);
            Vector3 point = vertexPath.GetPointAtDistance(dst);
            Quaternion rot = vertexPath.GetRotationAtDistance(dst);
            GameObject dropletGameObject = (GameObject)Instantiate(dropletPrefab, point, rot);
            //dropletGameObject.GetComponent<Rigidbody>().AddForce(directionAfterNoise);
            dropletGameObject.GetComponentInChildren<TrailRenderer>().time = timeLife;
            dropletGameObject.GetComponentInChildren<TrailRenderer>().widthMultiplier = width;
            dropletGameObject.GetComponent<Rigidbody>().velocity = directionAfterNoise;
            dropletGameObject.GetComponent<DropletComponent>().spawnPos = point;
            dropletGameObject.GetComponent<DropletComponent>().movingDirection = movingDirection;
            dropletGameObject.GetComponent<DropletComponent>().movingDirectionAfterNoise = directionAfterNoise;
            dropletGameObjects.Add(dropletGameObject);
            dst += spacing;
        }
        Destroy(dropletPath);

    }

    void Update()
    {
        UpdateStraightModeWithBezier();
    }

    void UpdateStraightModeWithBezier()
    {
        if (dropletGameObjects.Count > 0)
        {
            for (int i = 0; i < dropletGameObjects.Count; i++)
            {
                GameObject val = (GameObject)dropletGameObjects[i];
                movingDirection = val.GetComponent<DropletComponent>().movingDirection;
                if (val != null && val.activeInHierarchy)
                {
                    if (val.GetComponent<Rigidbody>().velocity.y > val.GetComponent<DropletComponent>().movingDirectionAfterNoise.y)
                    {
                        val.GetComponent<Rigidbody>().velocity = new Vector3(val.GetComponent<Rigidbody>().velocity.x, val.GetComponent<DropletComponent>().movingDirectionAfterNoise.y, 0f);
                    }

                    float TrailSizeX = Utils.TrailSizeX(timeLife, movingDirection.x);
                    float TrailSizeY = Utils.TrailSizeY(timeLife, movingDirection.y);
                    
                    if (movingDirection.x < 0f)
                    {

                        // Delete if droplet reach the bottom limit
                        if (val.transform.position.x < minX + TrailSizeX ||
                            val.transform.position.y < minY + TrailSizeY)
                        {
                            dropletGameObjects.Remove(val);
                            Destroy(val);
                        }
                    } else
                    {
                        // generate smoke
                        int smokeAmount = GameObject.FindGameObjectsWithTag("smoke").Length;
                        if (val.transform.position.y < minY && val.GetComponent<DropletComponent>().isCallNextSmoke == false && smokeAmount < 50)
                        {
                            GameObject.Find("SmokeLayer").GetComponent<SmokeManager>().InstanceSmoke(val.transform.position);
                            val.GetComponent<DropletComponent>().isCallNextSmoke = true;
                        }
                        // Delete if droplet reach the bottom limit
                        if (val.transform.position.x > maxX + TrailSizeX ||
                            val.transform.position.y < minY + TrailSizeY)
                        {
                            dropletGameObjects.Remove(val);
                            Destroy(val);
                        }
                    }
                }
            }
        }
    }
}
