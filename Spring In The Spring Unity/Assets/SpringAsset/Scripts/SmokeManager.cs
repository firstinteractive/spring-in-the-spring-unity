﻿using UnityEngine;

public class SmokeManager : MonoBehaviour
{
    public GameObject smokeRefab;
    public float smokePosVertical;

    public void InstanceSmoke(Vector3 pos)
    {
        Vector3 instancePos = new Vector3(pos.x, smokePosVertical, pos.z);
        GameObject smoke = Instantiate(smokeRefab, instancePos, Quaternion.identity);
    }
}
