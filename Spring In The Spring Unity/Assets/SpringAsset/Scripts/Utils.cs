﻿using UnityEngine;

public class Utils : MonoBehaviour
{
    public static float TrailSizeX(float t, float v)
    {
        return (Physics.gravity.x + v) * t;
    }
    public static float TrailSizeY(float t, float v)
    {
        return (Physics.gravity.y + v) * t;
    }

    public static Vector3 GenerateSpawnPostion(Vector3 direction, float minX, float maxX, float minY, float maxY, float minZ, float timeLife)
    {
        float x, y;
        Vector3 spawnPos; // There is 3 area made by changing velocity of x or y. z is not implement in this version.
        //Debug.Log("Direction to spawn: " + direction.x + ", " + direction.y + ", " + direction.z);
        if (System.Math.Abs(direction.x) < 0.01f && System.Math.Abs(direction.y) < 0.01f)
        {
            spawnPos = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), minZ);
        }
        else if (System.Math.Abs(direction.x) < 0.01f)
        {
            if (direction.y < 0f) // Falling down
            {
                y = Random.Range(maxY, maxY + System.Math.Abs(TrailSizeY(timeLife, direction.y)));
            }
            else // Going up
            {
                y = Random.Range(minY, minY - System.Math.Abs(TrailSizeY(timeLife, direction.y)));
            }

            x = Random.Range(minX, maxX);
            spawnPos = new Vector3(x, y, minZ);
        }
        else if (System.Math.Abs(direction.y) < 0.01f)
        {
            if (direction.x < 0f) // Going from max -> min
            {
                x = Random.Range(maxX, maxX + System.Math.Abs(TrailSizeX(timeLife, direction.x)));
            }
            else // Going from min -> max
            {
                x = Random.Range(minX, minX - System.Math.Abs(TrailSizeX(timeLife, direction.x)));
            }

            y = Random.Range(minY, maxY);
            spawnPos = new Vector3(x, y, minZ);
        }
        else
        {
            float square1, square2, square3;
            square1 = (maxX - minX) * Mathf.Abs(TrailSizeY(timeLife, direction.y));
            square2 = Mathf.Abs(TrailSizeX(timeLife, direction.x)) * Mathf.Abs(TrailSizeY(timeLife, direction.y));
            square3 = Mathf.Abs(TrailSizeX(timeLife, direction.x)) * (maxY - minY);
            float squareTotal = square1 + square2 + square3;
            float spawnInRange = Random.Range(0f, squareTotal); // random in total
            if (direction.x < 0f)
            {
                if (0 <= spawnInRange && spawnInRange < square1)
                {
                    //s1
                    x = Random.Range(minX, maxX);
                    y = Random.Range(maxY, maxY + System.Math.Abs(TrailSizeY(timeLife, direction.y)));

                }
                else if (square1 <= spawnInRange && spawnInRange < square1 + square2)
                {
                    //s2
                    x = Random.Range(maxX, maxX + System.Math.Abs(TrailSizeX(timeLife, direction.x)));
                    y = Random.Range(maxY, maxY + System.Math.Abs(TrailSizeY(timeLife, direction.y)));
                }
                else
                {
                    //s3
                    x = Random.Range(maxX, maxX + System.Math.Abs(TrailSizeX(timeLife, direction.x)));
                    y = Random.Range(minY, maxY);
                }
            }
            else
            {
                Debug.Log("maxY" + maxY);
                if (0 <= spawnInRange && spawnInRange < square1)
                {
                    //s1
                    x = Random.Range(minX, maxX);
                    y = Random.Range(maxY, maxY + System.Math.Abs(TrailSizeY(timeLife, direction.y)));

                }
                else if (square1 <= spawnInRange && spawnInRange < square1 + square2)
                {
                    //s2
                    x = Random.Range(minX - System.Math.Abs(TrailSizeX(timeLife, direction.x)), minX);
                    y = Random.Range(maxY, maxY + System.Math.Abs(TrailSizeY(timeLife, direction.y)));
                }
                else
                {
                    //s3
                    x = Random.Range(minX - System.Math.Abs(TrailSizeX(timeLife, direction.x)), minX);
                    //Debug.Log("XX" + x);
                    y = Random.Range(minY, maxY);
                }
            }
            spawnPos = new Vector3(x, y, minZ);
        }

        return spawnPos;
    }
}
