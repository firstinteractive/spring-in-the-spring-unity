﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BubbleManager : MonoBehaviour
{
    public GameObject bubblePrefab;
    public int amount = 100;
    public Vector3 movingDirection = new Vector3(0, -20, 0);
    public Vector3 movingNoiseByRatio = new Vector3(0, 0, 0);
    


    private ArrayList bubbleGameObjects = new ArrayList();
    private float minX = 0f;
    private float maxX = 0f;
    private float minY = 0f;
    private float maxY = 0f;
    private float minZ = 0f;
    private float maxZ = 0f;

    void Start()
    {
        minX = this.transform.position.x - this.transform.localScale.x / 2;
        minY = this.transform.position.y - this.transform.localScale.y / 2;
        minZ = this.transform.position.z - this.transform.localScale.z / 2;
        maxX = this.transform.position.x + this.transform.localScale.x / 2;
        maxY = this.transform.position.y + this.transform.localScale.y / 2;
        maxZ = this.transform.position.z + this.transform.localScale.z / 2;

        for (int i = 0; i < amount; i++)
        {
            CreateOne();
        }

    }

    void CreateOne()
    {
        CreateOneStraightMode();
    }

    void CreateOneStraightMode()
    {
        Vector3 directionAfterNoise = new Vector3(Random.Range(movingDirection.x * (1 - movingNoiseByRatio.x / 100f), movingDirection.x * (1 + movingNoiseByRatio.x / 100f)),
                Random.Range(movingDirection.y * (1 - movingNoiseByRatio.y / 100f), movingDirection.y * (1 + movingNoiseByRatio.y / 100f)),
                Random.Range(movingDirection.z * (1 - movingNoiseByRatio.z / 100f), movingDirection.z * (1 + movingNoiseByRatio.z / 100f)));

        Vector3 spawnPos = Utils.GenerateSpawnPostion(directionAfterNoise, minX, maxX, minY, maxY, minZ, bubblePrefab.transform.localScale.y);

        GameObject bubbleGameObject = (GameObject)Instantiate(bubblePrefab, spawnPos, Quaternion.identity);
        bubbleGameObject.GetComponent<Rigidbody>().AddForce(directionAfterNoise);
        bubbleGameObject.GetComponent<Rigidbody>().velocity = directionAfterNoise * Random.Range(0.01f, 1f);
        bubbleGameObject.GetComponent<BubbleComponent>().spawnPos = spawnPos;
        bubbleGameObject.GetComponent<BubbleComponent>().movingDirection = movingDirection;
        bubbleGameObject.GetComponent<BubbleComponent>().movingDirectionAfterNoise = directionAfterNoise;
        bubbleGameObjects.Add(bubbleGameObject);
    }

    void Update()
    {
        UpdateInStraightMode();
    }

    void UpdateInStraightMode()
    {
        if (bubbleGameObjects.Count > 0)
        {
            for (int i = 0; i < bubbleGameObjects.Count; i++)
            {
                GameObject val = (GameObject)bubbleGameObjects[i];
                if (val != null && val.activeInHierarchy)
                {
                    if (movingDirection.x < 0f)
                    {
                        if (
                        (
                            val.transform.position.y < val.GetComponent<BubbleComponent>().spawnPos.y - (maxY - minY) / 10) &&
                            val.GetComponent<BubbleComponent>().isCallNextSpawn == false)
                        {
                            CreateOne();
                            val.GetComponent<BubbleComponent>().isCallNextSpawn = true;
                        }
                        //if(val.GetComponent<BubbleComponent>().onCollision)
                        //{
                        //    Vector3 collisionPoint = val.GetComponent<BubbleComponent>().collisionPoint;
                        //    GameObject bubbleGameObject = (GameObject)Instantiate(bubblePrefab, collisionPoint, Quaternion.identity);
                        //    bubbleGameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(collisionPoint.x - 100, collisionPoint.x + 100), 50, 0));
                        //    bubbleGameObject.GetComponent<BubbleComponent>().spawnPos = collisionPoint;
                        //    bubbleGameObject.GetComponent<BubbleComponent>().isCallNextSpawn = true;
                        //    bubbleGameObjects.Add(bubbleGameObject);
                        //}

                        // Delete if Bubble reach the bottom limit
                        if (val.transform.position.x < minX ||
                            val.transform.position.y < minY)
                        {
                            bubbleGameObjects.Remove(val);
                            Destroy(val);
                        }
                    }
                    else
                    {
                        if (
                        (
                            val.transform.position.y < val.GetComponent<BubbleComponent>().spawnPos.y - (maxY - minY) / 10) &&
                            val.GetComponent<BubbleComponent>().isCallNextSpawn == false)
                        {
                            CreateOne();
                            val.GetComponent<BubbleComponent>().isCallNextSpawn = true;
                        }
                        //if (val.GetComponent<BubbleComponent>().onCollision && !val.GetComponent<BubbleComponent>().collisionGen)
                        //{
                        //    Vector3 collisionPoint = val.GetComponent<BubbleComponent>().collisionPoint;
                        //    GameObject bubbleGameObject = (GameObject)Instantiate(bubblePrefab, collisionPoint, Quaternion.identity);
                        //    bubbleGameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(collisionPoint.x - 75, collisionPoint.x + 75), 500, 0));
                        //    bubbleGameObject.GetComponent<BubbleComponent>().spawnPos = collisionPoint;
                        //    bubbleGameObject.GetComponent<BubbleComponent>().isCallNextSpawn = true;
                        //    bubbleGameObject.GetComponent<BubbleComponent>().collisionGen = true;
                        //    bubbleGameObjects.Add(bubbleGameObject);
                        //}
                        // Delete if Bubble reach the bottom limit
                        if (val.transform.position.x > maxX ||
                            val.transform.position.y < minY)
                        {
                            bubbleGameObjects.Remove(val);
                            Destroy(val);
                        }
                    }

                }
            }
        }
    }
}
