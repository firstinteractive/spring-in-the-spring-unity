﻿using UnityEngine;

public class BarrierGenerate : MonoBehaviour
{
    public GameObject barrierPrefab;
    public Camera mainCam;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Debug.Log("Input.mousePosition" + Input.mousePosition);
            Vector3 p = mainCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, mainCam.transform.position.z * -1));
            Debug.Log("Pos" + p);
            GameObject dropletGameObject = (GameObject)Instantiate(barrierPrefab, new Vector3(p.x, p.y, 0), Quaternion.Euler(90f, 0, 0));
        }
    }
}
