﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    public GameObject petal;
    private MeshFilter pistilMeshFilter;
    private Mesh pistilMesh;
    private Vector3[] verticesft;
    private Vector3[] nor;
    private Vector3 pistilPoint;
    private int verticeLength;
    // Start is called before the first frame update
    void Start()
    {
        pistilMeshFilter = gameObject.GetComponent<MeshFilter>();
        pistilMesh = pistilMeshFilter.mesh;
        verticesft = pistilMesh.vertices;
        verticeLength = verticesft.Length;
        pistilPoint = Vector3.zero;
        nor = pistilMesh.normals;
        for (int i = 0; i < verticeLength; i++)
        {
            pistilPoint = this.transform.TransformPoint(verticesft[i]);
            GameObject currentPetal = Instantiate(petal, pistilPoint, Quaternion.identity, this.transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
